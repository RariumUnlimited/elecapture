// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_GAME_GAMEHELPER_H_

#define SRC_GAME_GAMEHELPER_H_

#include <set>
#include <utility>

namespace EC {

/**
 * @brief Helper function for game class
 */
class GameHelper {
 public:
    virtual ~GameHelper() {}

    using Position = std::pair<unsigned int, unsigned int>;

    /**
     * @brief Get the list of possible tile that can be captured, playerTiles.size() + opponentTiles.size() must be < to Board::Width * Board::Height
     * @param playerTiles Tiles that are currently captured by the player
     * @param opponentTiles Tiles that are currently captured by the opponent
     * @return The list of tiles that can be captured by the player
     */
    virtual std::set<Position> GetPossibleCapture(std::set<Position> playerTiles,
                                                  std::set<Position> opponentTiles) const = 0;

    /**
     * @brief Return true if the player won the game
     * @param playerTiles Tiles of the player
     * @param opponentTiles Tiles of the opponent
     * @return True if the player won the game
     */
    virtual bool DidIWin(std::set<Position> playerTiles,
                         std::set<Position> opponentTiles) const = 0;

    virtual Board::Tile GetPlayerTotalElements(std::set<Position> playerTiles) const = 0;
};

}  // namespace EC

#endif  // SRC_GAME_GAMEHELPER_H_
