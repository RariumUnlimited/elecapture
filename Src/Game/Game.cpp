// Copyright RariumUnlimited - Licence : MIT
#include "Game/Game.h"

#include <chrono>
#include <functional>
#include <future>

#include <Rarium/Tools/Math.h>

namespace EC {

Game::Game(const Board& board, Player* player1, Player* player2) :
    board(board),
    player1(player1),
    player2(player2) {
    assert(player1 && player2);
    player1Tiles.insert({(Board::Width - 1) / 2, Board::Height - 1});
    player2Tiles.insert({(Board::Width - 1) / 2, 0});
    player1->NotifyCapture(this, player1Tiles, player2Tiles);
    player2->NotifyCapture(this, player2Tiles, player1Tiles);
}

void Game::Run() {
    run = true;

    bool player1turn = true;

    while (run) {
        std::set<Position> possibleCapture;  // List of possible capture for current player
        std::function<Player::Position()> turnFunction;  // Will hold either player1.TakeTurn or player2.TakeTurn depending on which turn it is

        // Assign the right method depending on which player turn it is
        if (player1turn) {
            possibleCapture = GetPossibleCapture(player1Tiles, player2Tiles);
            turnFunction = std::bind(&Player::TakeTurn,
                                     player1,
                                     board,
                                     this,
                                     possibleCapture,
                                     player1Tiles,
                                     player2Tiles);
        } else {
            possibleCapture = GetPossibleCapture(player2Tiles, player1Tiles);
            turnFunction = std::bind(&Player::TakeTurn,
                                     player2,
                                     board,
                                     this,
                                     possibleCapture,
                                     player2Tiles,
                                     player1Tiles);
        }

        Position playedPosition = turnFunction();

        if (possibleCapture.find(playedPosition) != possibleCapture.end()) {
            if (player1turn)
                player1Tiles.insert(playedPosition);
            else
                player2Tiles.insert(playedPosition);

            player1turn = !player1turn;
            player1->NotifyCapture(this, player1Tiles, player2Tiles);
            player2->NotifyCapture(this, player2Tiles, player1Tiles);
        }

        if (player1Tiles.size() + player2Tiles.size() ==
                Board::Width * Board::Height) {
            if (DidIWin(player1Tiles, player2Tiles)) {
               player1->NotifyGameEnd(this, true);
               player2->NotifyGameEnd(this, false);
            } else {
                player1->NotifyGameEnd(this, false);
                player2->NotifyGameEnd(this, true);
            }

            run = false;
        }
    }
}

bool Game::DidIWin(std::set<Position> playerTiles,
                   std::set<Position> opponentTiles) const {
    if (playerTiles.size() + opponentTiles.size() < Board::Width * Board::Height)
        return false;

    Board::Tile playerElements = GetPlayerTotalElements(playerTiles);
    Board::Tile opponentElements = GetPlayerTotalElements(opponentTiles);

    unsigned int playerMostElement = 0;
    unsigned int opponentMostElement = 0;

    if (playerElements.Fire > opponentElements.Fire)
        ++playerMostElement;
    else if (opponentElements.Fire > playerElements.Fire)
        ++opponentMostElement;

    if (playerElements.Water > opponentElements.Water)
        ++playerMostElement;
    else if (opponentElements.Water > playerElements.Water)
        ++opponentMostElement;

    if (playerElements.Wind > opponentElements.Wind)
        ++playerMostElement;
    else if (opponentElements.Wind > playerElements.Wind)
        ++opponentMostElement;

    if (playerElements.Earth > opponentElements.Earth)
        ++playerMostElement;
    else if (opponentElements.Earth > playerElements.Earth)
        ++opponentMostElement;

    if (playerElements.Metal > opponentElements.Metal)
        ++playerMostElement;
    else if (opponentElements.Metal > playerElements.Metal)
        ++opponentMostElement;

    if (playerMostElement > opponentMostElement)
        return true;
    else
        return false;
}

Board::Tile Game::GetPlayerTotalElements(std::set<Position> playerTiles) const {
    Board::Tile playerElements;

    for (const Position& p : playerTiles) {
        const Board::Tile& t = board.T(p.first, p.second);
        playerElements.Fire += t.Fire;
        playerElements.Water += t.Water;
        playerElements.Wind += t.Wind;
        playerElements.Earth += t.Earth;
        playerElements.Metal += t.Metal;
    }

    return playerElements;
}

std::set<Game::Position> Game::GetPossibleCapture(std::set<Position> playerTiles,
                                                  std::set<Position> opponentTiles) const {
    assert(playerTiles.size() + opponentTiles.size() < Board::Width * Board::Height);

    std::set<Position> result;
    std::set<Position> freeTiles;

    for (auto i = 0; i < Board::Width; ++i) {
        for (auto j = 0; j < Board::Height; ++j) {
            Position p = {i, j};

            if (playerTiles.find(p) == playerTiles.end() &&
                opponentTiles.find(p) == opponentTiles.end()) {
                freeTiles.insert(p);
            }
        }
    }

    for (const Position& p : playerTiles) {
        for (auto i = RR::Clamp<int>(p.first - 1, 0, Board::Width - 1);
                  i <= RR::Clamp<int>(p.first + 1, 0, Board::Width - 1);
                ++i) {
            Position pTemp = {i, p.second};
            if (freeTiles.find(pTemp) != freeTiles.end())
                result.insert(pTemp);
        }

        unsigned int min = RR::Clamp<int>(p.second - 1, 0, Board::Height - 1);
        unsigned int max = RR::Clamp<int>(p.second + 1, 0, Board::Height - 1);
        for (auto j = RR::Clamp<int>(p.second - 1, 0, Board::Height - 1);
                  j <= RR::Clamp<int>(p.second + 1, 0, Board::Height - 1);
                ++j) {
            Position pTemp = {p.first, j};
            if (freeTiles.find(pTemp) != freeTiles.end())
                result.insert(pTemp);
        }
    }

    if (result.size() > 0)
        return result;
    else
        return freeTiles;
}

}  // namespace EC
