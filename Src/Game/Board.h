// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_GAME_BOARD_H_

#define SRC_GAME_BOARD_H_

#include <array>
#include <cassert>

#include <SFML/System.hpp>

namespace EC {

/**
 * @brief The board of the game
 */
class Board {
 public:
    /**
     * @brief Construct a new board
     */
    Board();

    /**
     * @brief The Tile struct
     */
    struct Tile {
        unsigned int Fire = 0;
        unsigned int Water = 0;
        unsigned int Wind = 0;
        unsigned int Earth = 0;
        unsigned int Metal = 0;
    };

    static constexpr unsigned int Width = 5;  ///< Width of the board
    static constexpr unsigned int Height = 8;  ///< Height of the board
    static constexpr unsigned int MaxElementAmount = 200;  ///< Max amount of each element on a tile

    using Tiles = std::array<Tile, Width * Height>;

    /// Get the tiles of the board
    inline const Tiles& GetTiles() const { return tiles; }

    /// Get a tile of the board
    inline const Tile& T(unsigned int x, unsigned int y) const {
        assert(x >= 0 && y >= 0 && x < Width && y < Height);
        return tiles[x + y * Width];
    }

 private:
    Tiles tiles;  ///< Tile array of the board
};

}  // namespace EC

#endif  // SRC_GAME_BOARD_H_
