// Copyright RariumUnlimited - Licence : MIT
#include <thread>
#include <iostream>

#include <Rarium/Tools/Log.h>

#include "Game/Board.h"
#include "Game/Game.h"
#include "IA/MC.h"
#include "IA/Random.h"
#include "Window/Window.h"

int main(int argc, char* argv[]) {
    RR::Logger::Log(false).AddStream(&std::cout);

    EC::Board b;
    EC::Window w(b);
    EC::MC iaMC;

    EC::Game game(b, &w, &iaMC);
    std::thread t(&EC::Game::Run, &game);

    w.CloseEvent += new RR::Event<void>::T<EC::Game>(&game, &EC::Game::Stop);
    w.CloseEvent += new RR::Event<void>::T<EC::Window>(&w, &EC::Window::OnClose);  // Defined here, because order matters, we want EC::Window::OnClose to be called avec EC::Game::Stop

    w.Run();

    t.join();
}

