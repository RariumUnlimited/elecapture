// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_IA_MC_H_

#define SRC_IA_MC_H_

#include <map>
#include <random>
#include <set>

#include "Game/Player.h"

namespace EC {

/**
 * @brief A Monte-Carlo IA
 */
class MC : public Player {
 public:
    /**
     * @brief Construct a new Monte-Carlo IA
     */
    MC();

    /**
     * @brief TakeTurn Will choose a tile to capture
     * @param b The board of the game
     * @param possibleCapture List of tile that the player can capture during the turn
     * @param yourTiles The tiles already captured by the player
     * @param opponentTiles The tiles already captured by the opponent
     * @return The position of the tile that the player wants to capture
     */
    Position TakeTurn(const Board& b,
                      const GameHelper* helper,
                      const std::set<Position>& possibleCapture,
                      const std::set<Position>& yourTiles,
                      const std::set<Position>& opponentTiles) override;

 private:
    /**
     * @brief Play a game
     * @param helper GameHelper
     * @param yourTiles The tiles already captured by the IA
     * @param opponentTiles The tiles already captured by the opponent
     * @param IATurn True if the current turn is the ia one
     * @return True if we won the game
     */
    bool PlayGame(const GameHelper* helper,
                  const std::set<Position>& yourTiles,
                  const std::set<Position>& opponentTiles,
                  bool IATurn);


    static constexpr unsigned int SimulationCount = 10000;
    std::mt19937 generator;  ///< Pseudo random number generator for this IA
};

}  // namespace EC

#endif  // SRC_IA_MC_H_
