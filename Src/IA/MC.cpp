// Copyright RariumUnlimited - Licence : MIT
#include "IA/MC.h"

#include <omp.h>

#include <atomic>
#include <chrono>
#include <utility>
#include <vector>

#include <Rarium/Tools/Log.h>

namespace EC {

MC::MC() {
    generator = std::mt19937(
                static_cast<unsigned int>(
                    std::chrono::system_clock::now().time_since_epoch().count()));
}

Player::Position MC::TakeTurn(const Board& b,
                                  const GameHelper* helper,
                                  const std::set<Position>& possibleCapture,
                                  const std::set<Position>& yourTiles,
                                  const std::set<Position>& opponentTiles) {
    RR::Logger::Log() << "IA started playing !\n";

    std::vector<Position> captureVector(possibleCapture.begin(), possibleCapture.end());
    std::vector<std::set<Position>> newYourTilesVector(captureVector.size());
    std::vector<std::pair<std::atomic_uint, std::atomic_uint>> playWinVector(captureVector.size());  // <Play, Win>

    for (auto i = 0; i < captureVector.size(); ++i) {
        newYourTilesVector[i] = yourTiles;
        newYourTilesVector[i].insert(captureVector[i]);
        playWinVector[i].first = 0;
        playWinVector[i].second = 0;
    }

    #pragma omp parallel for
    for (int i = 0; i < SimulationCount; ++i) {
        using UIntDistrib = std::uniform_int_distribution<unsigned int>;
        unsigned int play = UIntDistrib(0, captureVector.size() - 1)(generator);
        bool won = PlayGame(helper, newYourTilesVector[play], opponentTiles, false);
        ++playWinVector[play].first;
        if (won)
            ++playWinVector[play].second;
    }

    float currentRatio = 0.f;
    Position bestPosition = {0, 0};

    for (auto i = 0; i < captureVector.size(); i++) {
        float ratio = static_cast<float>(playWinVector[i].second) /
                      static_cast<float>(playWinVector[i].first);
        if (ratio >= currentRatio) {
            currentRatio = ratio;
            bestPosition = captureVector[i];
        }
    }

    RR::Logger::Log() << "IA finished playing !\n";

    return bestPosition;
}

bool MC::PlayGame(const GameHelper* helper,
                  const std::set<Position>& yourTiles,
                  const std::set<Position>& opponentTiles,
                  bool IATurn) {
    bool won;

    if (yourTiles.size() + opponentTiles.size() == Board::Width * Board::Height) {
        won = helper->DidIWin(yourTiles, opponentTiles);
    } else {
        Position nextCapture;
        {
            std::set<Position> possibleCapture;
            if (IATurn)
                possibleCapture = helper->GetPossibleCapture(yourTiles,
                                                             opponentTiles);
            else
                possibleCapture = helper->GetPossibleCapture(opponentTiles,
                                                             yourTiles);

            std::vector<Position> captureVector(possibleCapture.begin(),
                                                possibleCapture.end());
            using UIntDistrib = std::uniform_int_distribution<unsigned int>;
            unsigned int play = UIntDistrib(0, captureVector.size() - 1)(generator);
            nextCapture = captureVector[play];
        }

        std::set<Position> newYourTiles = yourTiles;
        std::set<Position> newOpponentTiles = opponentTiles;
        if (IATurn)
            newYourTiles.insert(nextCapture);
        else
            newOpponentTiles.insert(nextCapture);

        won = PlayGame(helper, newYourTiles, newOpponentTiles, !IATurn);
    }

    return won;
}

}  // namespace EC
